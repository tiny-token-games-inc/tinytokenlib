#pragma once

#include "TinyTokenLib/Public/Core/Macros/TT_Macros.h"
#include "TinyTokenLib/Public/Core/Types/TT_Types_UE.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_CoreMinimal.h"

#include "TT_Core_Utils_UE.generated.h"

// --------------------------------------------------------------------------------------------------------------------

UCLASS()
class UTT_Core_Utils_UE : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    TT_GENERATED_BODY(UTT_Core_Utils_UE);

public:
};
