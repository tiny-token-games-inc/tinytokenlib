#pragma once

#include "Core/Format/TT_Format.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_CoreMinimal.h"

// --------------------------------------------------------------------------------------------------------------------

DECLARE_LOG_CATEGORY_EXTERN(TinyTokenCore, Log, All);

// --------------------------------------------------------------------------------------------------------------------

namespace tt { namespace core { namespace log {

// --------------------------------------------------------------------------------------------------------------------
// basic logging

template <typename T, typename ... TArgs>
auto
    Fatal(const T&  InString,
              TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, Fatal, TEXT("[TT_Lib] %s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T, typename ... TArgs>
auto
    Error(const T&  InString,
              TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, Error, TEXT("[TT_Lib] %s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T, typename ... TArgs>
auto
    Warning(const T&  InString,
                TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, Warning, TEXT("[TT_Lib] %s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T, typename ... TArgs>
auto
    Display(const T&  InString,
                TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, Display, TEXT("[TT_Lib]%s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T, typename ... TArgs>
auto
    Log(const T&  InString,
        TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, Log, TEXT("[TT_Lib]%s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T, typename ... TArgs>
auto
    Verbose(const T&  InString,
                TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, Verbose, TEXT("[TT_Lib]%s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T, typename ... TArgs>
auto
    VeryVerbose(const T&  InString,
                    TArgs&&...InArgs)
    -> void
{
    UE_LOG(TinyTokenCore, VeryVerbose, TEXT("[TT_Lib]%s"), *tt::Format_UE(InString, InArgs...));
}

// --------------------------------------------------------------------------------------------------------------------
// logging with expression

template <typename T, typename ... TArgs>
auto
    Fatal(bool      InExpression,
              const T&  InString,
              TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { Fatal(InString, InArgs...); }
}

template <typename T, typename ... TArgs>
auto
    Error(bool      InExpression,
              const T&  InString,
              TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { Error(InString, InArgs...); }
}

template <typename T, typename ... TArgs>
auto
    Warning(bool      InExpression,
                const T&  InString,
                TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { UE_LOG(TinyTokenCore, Warning, TEXT("%s"), *tt::format(InString, InArgs...)); }
}

template <typename T, typename ... TArgs>
auto
    Display(bool      InExpression,
                const T&  InString,
                TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { Display(InString, InArgs...); }
}

template <typename T, typename ... TArgs>
auto
    Log(bool      InExpression,
        const T&  InString,
        TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { Log(InString, InArgs...); }
}

template <typename T, typename ... TArgs>
auto
    Verbose(bool      InExpression,
                const T&  InString,
                TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { Verbose(InString, InArgs...); }
}

template <typename T, typename ... TArgs>
auto
    VeryVerbose(bool      InExpression,
                    const T&  InString,
                    TArgs&&...InArgs)
    -> void
{
    if (InExpression)
    { VeryVerbose(InString, InArgs...); }
}

}}}
