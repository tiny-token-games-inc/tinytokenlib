#include "TT_Log_UE.h"

#include "TinyTokenLib/Public/Core/Log/TT_Log.h"

// --------------------------------------------------------------------------------------------------------------------

auto
    UTT_Core_Log::
    Log_Fatal(FText InMsg)
    -> void
{
    ETT_Core_ValidInvalid temp;
    Log_Fatal_If(true, InMsg, temp);
}

auto
    UTT_Core_Log::
    Log_Error(FText InMsg)
    -> void
{
    ETT_Core_ValidInvalid temp;
    Log_Error_If(true, InMsg, temp);
}

auto
    UTT_Core_Log::
    Log_Warning(FText InMsg)
    -> void
{

    ETT_Core_ValidInvalid temp;
    Log_Warning_If(true, InMsg, temp);
}

auto
    UTT_Core_Log::
    Log_Display(FText InMsg)
    -> void
{

    ETT_Core_ValidInvalid temp;
    Log_Display_If(true, InMsg, temp);
}

auto
    UTT_Core_Log::
    Log(FText InMsg)
    -> void
{

    ETT_Core_ValidInvalid temp;
    Log_If(true, InMsg, temp);
}

auto
    UTT_Core_Log::
    Log_Verbose(FText InMsg)
    -> void
{

    ETT_Core_ValidInvalid temp;
    Log_Verbose_If(true, InMsg, temp);
}

auto
    UTT_Core_Log::
    Log_VeryVerbose(FText InMsg)
    -> void
{
    ETT_Core_ValidInvalid temp;
    Log_VeryVerbose_If(true, InMsg, temp);
}

// --------------------------------------------------------------------------------------------------------------------

auto
    UTT_Core_Log::
    Log_Fatal_If(bool                   InExpression,
                 FText                  InMsg,
                 ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (InExpression)
    {
        tt::core::log::Fatal(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}

auto
    UTT_Core_Log::
    Log_Error_If(bool                   InExpression,
                 FText                  InMsg,
                 ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (InExpression)
    {
        tt::core::log::Error(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}

auto
    UTT_Core_Log::
    Log_Warning_If(bool                   InExpression,
                   FText                  InMsg,
                   ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (InExpression)
    {
        tt::core::log::Warning(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}

auto
    UTT_Core_Log::
    Log_Display_If(bool                   InExpression,
                   FText                  InMsg,
                   ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (InExpression)
    {
        tt::core::log::Display(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}

auto
    UTT_Core_Log::
    Log_Verbose_If(bool                   InExpression,
                   FText                  InMsg,
                   ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (InExpression)
    {
        tt::core::log::Verbose(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}

auto
    UTT_Core_Log::
    Log_VeryVerbose_If(bool                   InExpression,
                       FText                  InMsg,
                       ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (InExpression)
    {
        tt::core::log::VeryVerbose(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}

auto
    UTT_Core_Log::
    Log_If(bool                   InExpression,
          FText                  InMsg,
          ETT_Core_ValidInvalid& OutValidInvalid) -> void
{
    if (InExpression)
    {
        tt::core::log::Log(InExpression, InMsg);
        OutValidInvalid = ETT_Core_ValidInvalid::Valid;
    }
    else
    {
        OutValidInvalid = ETT_Core_ValidInvalid::Invalid;
    }
}
