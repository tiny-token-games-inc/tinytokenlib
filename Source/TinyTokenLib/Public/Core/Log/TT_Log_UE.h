#pragma once

#include "TinyTokenLib/Public/Core/Enums/TT_Enums_UE.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_CoreMinimal.h"

#include "TT_Log_UE.generated.h"

// --------------------------------------------------------------------------------------------------------------------

UCLASS()
class UTT_Core_Log : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:

    UFUNCTION(BlueprintCallable)
    static void Log_Fatal(FText InMsg);

    UFUNCTION(BlueprintCallable)
    static void Log_Error(FText InMsg);

    UFUNCTION(BlueprintCallable)
    static void Log_Warning(FText InMsg);

    UFUNCTION(BlueprintCallable)
    static void Log_Display(FText InMsg);

    UFUNCTION(BlueprintCallable)
    static void Log(FText InMsg);

    UFUNCTION(BlueprintCallable)
    static void Log_Verbose(FText InMsg);

    UFUNCTION(BlueprintCallable)
    static void Log_VeryVerbose(FText InMsg);

public:

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_Fatal_If(bool                   InExpression,
                             FText                  InMsg,
                             ETT_Core_ValidInvalid& OutValidInvalid);

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_Error_If(bool                   InExpression,
                             FText                  InMsg,
                             ETT_Core_ValidInvalid& OutValidInvalid);

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_Warning_If(bool                   InExpression,
                               FText                  InMsg,
                               ETT_Core_ValidInvalid& OutValidInvalid);

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_Display_If(bool                   InExpression,
                               FText                  InMsg,
                               ETT_Core_ValidInvalid& OutValidInvalid);

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_If(bool                   InExpression,
                       FText                  InMsg,
                       ETT_Core_ValidInvalid& OutValidInvalid);

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_Verbose_If(bool                   InExpression,
                               FText                  InMsg,
                               ETT_Core_ValidInvalid& OutValidInvalid);

    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void Log_VeryVerbose_If(bool                   InExpression,
                                   FText                  InMsg,
                                   ETT_Core_ValidInvalid& OutValidInvalid);

public:
};
