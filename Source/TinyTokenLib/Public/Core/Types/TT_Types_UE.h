#pragma once

#include "TinyTokenLib/Public/Core/Macros/TT_Macros.h"
#include "TinyTokenLib/Public/Core/Format/TT_Format.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_CoreMinimal.h"

#include "TT_Types_UE.generated.h"

// ----------------------------------------------------------------------------------------------------------------

USTRUCT(BlueprintType)
struct TINYTOKENLIB_API FTT_Coords
{
    GENERATED_BODY()

public:
    FTT_Coords() = default;
    FTT_Coords(int32 InX, int32 InY);

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 _X = 0;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    int32 _Y = 0;

public:
    TT_PROPERTY_GET(_X);
    TT_PROPERTY_GET(_Y);
};

TT_DEFINE_CUSTOM_FORMATTER(FTT_Coords, [&]()
{
    return tt::Format(TEXT("{},{}"), InObj.Get_X(), InObj.Get_Y());
});
