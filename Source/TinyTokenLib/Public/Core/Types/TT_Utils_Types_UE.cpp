#include "TT_Utils_Types_UE.h"

#include "TinyTokenLib/Public/Core/Format/TT_Format.h"
#include "TinyTokenLib/Public/Core/Ensure/TT_Ensure_UE.h"

// --------------------------------------------------------------------------------------------------------------------

auto
    UTT_Utils_Types_UE::
    Add_Coord(FTT_Coords InA,
              FTT_Coords InB)
    -> FTT_Coords
{
    return FTT_Coords
    {
        InA.Get_X() + InB.Get_X(),
        InA.Get_Y() + InB.Get_Y()
    };
}

auto
    UTT_Utils_Types_UE::
    Subtract_Coord(FTT_Coords InA,
                   FTT_Coords InB)
    -> FTT_Coords
{
    return FTT_Coords
    {
        InA.Get_X() - InB.Get_X(),
        InA.Get_Y() - InB.Get_Y()
    };
}

auto
    UTT_Utils_Types_UE::
    Multiply_Coord(FTT_Coords InA,
                   FTT_Coords InB)
    -> FTT_Coords
{
    return FTT_Coords
    {
        InA.Get_X() * InB.Get_X(),
        InA.Get_Y() * InB.Get_Y()
    };
}

auto
    UTT_Utils_Types_UE::
    Divide_Coord(FTT_Coords InA,
                 FTT_Coords InB)
    -> FTT_Coords
{
    if (! tt::core::ensure::EnsureMsgf(InB.Get_X() != 0 && InB.Get_Y() != 0,
                                       TEXT("Divide by zero for InA[{}] and InB[{}]"),
                                       InA,
                                       InB))
    { return {}; }

    return FTT_Coords
    {
        InA.Get_X() / InB.Get_X(),
        InA.Get_Y() / InB.Get_Y()
    };
}

auto
    UTT_Utils_Types_UE::
    Get_Coord_Type(FTT_Coords InCoords)
    -> ETT_Core_ZeroPositiveNegative
{
    if (InCoords.Get_X() == 0 &&
        InCoords.Get_Y() == 0)
    {
        return ETT_Core_ZeroPositiveNegative::Zero;
    }
    else if (InCoords.Get_X() < 0 ||
             InCoords.Get_Y() < 0)
    {
        return ETT_Core_ZeroPositiveNegative::Negative;
    }
    else
    {
        return ETT_Core_ZeroPositiveNegative::Positive;
    }
}

// --------------------------------------------------------------------------------------------------------------------

auto
    UTT_Utils_Types_UE::
    ToString_Coord(FTT_Coords InCoords)
    -> FString
{
    return tt::Format_UE(TEXT("{}"), InCoords);
}

auto
    UTT_Utils_Types_UE::
    ToText_Coord(FTT_Coords InCoords)
    -> FText
{
    return FText::FromString(ToString_Coord(InCoords));
}
