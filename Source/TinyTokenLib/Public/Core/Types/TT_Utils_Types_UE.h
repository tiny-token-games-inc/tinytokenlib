#pragma once

#include "TinyTokenLib/Public/Core/Macros/TT_Macros.h"
#include "TinyTokenLib/Public/Core/Types/TT_Types_UE.h"
#include "TinyTokenLib/Public/Core/Enums/TT_Enums_UE.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_BlueprintFunctionLibrary.h"

#include "TT_Utils_Types_UE.generated.h"

// --------------------------------------------------------------------------------------------------------------------

UCLASS()
class TINYTOKENLIB_API UTT_Utils_Types_UE
    : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    TT_GENERATED_BODY(UTT_Utils_Types_UE);

public:
    UFUNCTION(BlueprintPure,
        meta=(CompactNodeTitle="+"))
    static FTT_Coords
    Add_Coord(FTT_Coords InA, FTT_Coords InB);

    UFUNCTION(BlueprintPure,
        meta=(CompactNodeTitle="-"))
    static FTT_Coords
    Subtract_Coord(FTT_Coords InA, FTT_Coords InB);

    UFUNCTION(BlueprintPure,
        meta=(CompactNodeTitle="*"))
    static FTT_Coords
    Multiply_Coord(FTT_Coords InA, FTT_Coords InB);

    UFUNCTION(BlueprintPure,
        meta=(CompactNodeTitle="/"))
    static FTT_Coords
    Divide_Coord(FTT_Coords InA, FTT_Coords InB);

    UFUNCTION(BlueprintPure)
    static ETT_Core_ZeroPositiveNegative
    Get_Coord_Type(FTT_Coords InCoords);

    UFUNCTION(BlueprintPure,
        meta=(DisplayName="ToString"))
    static FString
    ToString_Coord(FTT_Coords InCoords);

    UFUNCTION(BlueprintPure,
        meta=(DisplayName="ToText"))
    static FText
    ToText_Coord(FTT_Coords InCoords);
};
