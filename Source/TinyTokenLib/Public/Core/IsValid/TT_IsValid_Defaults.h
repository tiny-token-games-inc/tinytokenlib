#pragma once

#include "TinyTokenLib/Public/Engine/TT_Engine_CoreMinimal.h"

// --------------------------------------------------------------------------------------------------------------------

namespace tt { namespace core {

// --------------------------------------------------------------------------------------------------------------------

    template <typename T, typename T_Lambda>
    class IsValid_Executor
    {
    public:
        auto IsValid(const T& InObj) -> bool
        {
            return IsValid(InObj);
        }
    };

// --------------------------------------------------------------------------------------------------------------------

    template <typename T, typename T_Validator>
    auto
    IsValid(

}}

#define TT_DEFINE_CUSTOM_IS_VALID(_type_)\
namespace tt { core {\
    \
    template <typename T>\
    class IsValid_Executor\
    {\
    public:\
        auto IsValid(const T& InObj) -> bool\
        {\
            return IsValid(InObj);\
        }\
    };\
\
}}\
