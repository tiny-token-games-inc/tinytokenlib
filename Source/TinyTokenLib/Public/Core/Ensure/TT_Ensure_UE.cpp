#include "TT_Ensure_UE.h"

auto
    UTT_Core_Ensure::
    EnsureMsgf(bool                   InExpression,
               FText                  InMsg,
               ETT_Core_ValidInvalid& OutValidInvalid)
    -> void
{
    if (EnsureMsgf(InExpression, TEXT("{}"), InMsg.ToString()))
    { OutValidInvalid = ETT_Core_ValidInvalid::Valid; }
    else
    { OutValidInvalid = ETT_Core_ValidInvalid::Invalid; }
}
