#pragma once

#include "TinyTokenLib/Public/Core/Format/TT_Format.h"
#include "TinyTokenLib/Public/Core/Enums/TT_Enums_UE.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_BlueprintFunctionLibrary.h"

#include "TT_Ensure_UE.generated.h"

// --------------------------------------------------------------------------------------------------------------------

UCLASS()
class TINYTOKENLIB_API UTT_Core_Ensure
    : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintCallable,
        Meta = (ExpandEnumAsExecs="OutValidInvalid"))
    static void EnsureMsgf(bool                   InExpression,
                           FText                  InMsg,
                           ETT_Core_ValidInvalid& OutValidInvalid);

public:
    template <typename T, typename ... TArgs>
    static auto EnsureMsgf(bool      InExpression,
                           const T&  InString,
                           TArgs&&...InArgs) -> bool;

    template <typename T>
    static auto Ensuref_InvalidEnum(T InEnumValue) -> void;
};

// --------------------------------------------------------------------------------------------------------------------

template <typename T, typename ... TArgs>
auto
    UTT_Core_Ensure::
    EnsureMsgf(bool      InExpression,
               const T&  InString,
               TArgs&&...InArgs)
    -> bool
{
    return ensureMsgf(InExpression, TEXT("%s"), *tt::Format_UE(InString, InArgs...));
}

template <typename T>
auto
    UTT_Core_Ensure::
    Ensuref_InvalidEnum(T InEnumValue)
    -> void
{
    EnsureMsgf(false, TEXT("Encountered an invalid value for Enum [{}]"), InEnumValue);
}

// --------------------------------------------------------------------------------------------------------------------

namespace tt { namespace core {

    using ensure = UTT_Core_Ensure;

}}
