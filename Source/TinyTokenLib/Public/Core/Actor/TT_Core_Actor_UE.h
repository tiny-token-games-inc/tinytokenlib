#pragma once

#include "TinyTokenLib/Public/Core/Macros/TT_Macros.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_Actor.h"

#include "TT_Core_Actor_UE.generated.h"

UCLASS(Abstract, BlueprintType, Blueprintable)
class TINYTOKENLIB_API ATT_Core_Actor_UE
    : public AActor
{
	GENERATED_BODY()

public:
    TT_GENERATED_BODY(ATT_Core_Actor_UE);

private:
    auto OnConstruction(const FTransform& InTransform) -> void override;
};
