#include "TT_Core_ActorComponent_UE.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_Actor.h"

// ----------------------------------------------------------------------------------------------------------------

FTT_ActorComponent_DoConstruct_Params::
FTT_ActorComponent_DoConstruct_Params(const AActor* InActor, const FTransform& InTransform)
    : _Actor(InActor)
    , _Transform(InTransform)
{ }

// ----------------------------------------------------------------------------------------------------------------

auto
    UTT_Core_ActorComponent_UE::
    PostInitProperties() 
    -> void
{
    Super::PostInitProperties();

    if (NOT IsValid(GetOwner()))
    { return; }

    Do_Construct_Implementation
    (
        FTT_ActorComponent_DoConstruct_Params
        {
            GetOwner(),
            GetOwner()->GetTransform()
        }
    );
}

auto 
    UTT_Core_ActorComponent_UE::
    Do_Construct_Implementation(const FTT_ActorComponent_DoConstruct_Params& InParams)
    -> void
{
    Do_Construct(InParams);
}
