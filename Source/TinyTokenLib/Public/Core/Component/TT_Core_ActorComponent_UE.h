#pragma once

#include "TinyTokenLib/Public/Core/Macros/TT_Macros.h"
#include <TinyTokenLib/Public/Engine/TT_Engine_ActorComponent.h>

#include <CoreMinimal.h>

#include "TT_Core_ActorComponent_UE.generated.h"

// ----------------------------------------------------------------------------------------------------------------

USTRUCT(BlueprintType)
struct TINYTOKENLIB_API FTT_ActorComponent_DoConstruct_Params
{
    GENERATED_BODY()

public:
    TT_GENERATED_BODY(FTT_ActorComponent_DoConstruct_Params);

public:
    FTT_ActorComponent_DoConstruct_Params() = default;
    FTT_ActorComponent_DoConstruct_Params(const AActor* InActor, const FTransform& InTransform);

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    const AActor*   _Actor = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FTransform      _Transform;

public:
    TT_PROPERTY_GET(_Actor);
    TT_PROPERTY_GET(_Transform);
};

// ----------------------------------------------------------------------------------------------------------------

UCLASS(Abstract, BlueprintType, Blueprintable)
class TINYTOKENLIB_API UTT_Core_ActorComponent_UE
    : public UActorComponent
{
	GENERATED_BODY()

public:
    auto
    PostInitProperties() -> void override;

public:
    UFUNCTION(BlueprintImplementableEvent,
              Category = "TinyTokens|Core|Components",
              meta = (DisplayName = "ConstructionScript"))
    void
    Do_Construct (const FTT_ActorComponent_DoConstruct_Params& InParams);

private:
    virtual auto
    Do_Construct_Implementation(const FTT_ActorComponent_DoConstruct_Params& InActor) -> void;
};
