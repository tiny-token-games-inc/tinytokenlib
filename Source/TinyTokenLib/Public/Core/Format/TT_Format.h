#pragma once

#include "Windows/AllowWindowsPlatformTypes.h"
#include "Windows/PreWindowsApi.h"

#define FMT_HEADER_ONLY
#define FMT_EXCEPTIONS 0
#include "3rdParty/fmt/include/fmt/format.h"

namespace tt {

template <typename T, typename ... TArgs>
auto
    Format_UE(const T&  InString,
              TArgs&&...InArgs) -> FString
{
    // return fmt::format(InString, InArgs);
    fmt::format(TEXT(""));
    return FString{ };
}

template <typename T, typename ... TArgs>
auto
    Format(const T&  InString,
           TArgs&&...InArgs) -> std::basic_string<wchar_t>
{
    return fmt::format(InString, InArgs...);
}

}

// --------------------------------------------------------------------------------------------------------------------

#define TT_DEFINE_CUSTOM_FORMATTER(_Type_, _Lambda_)\
template<>\
struct fmt::formatter<_Type_>\
{\
    template<typename ParseContext>\
    constexpr auto parse(ParseContext& ctx)\
    { return ctx.begin(); }\
\
    template<typename FormatContext>\
    auto format(_Type_ const& InObj, FormatContext& ctx)\
    {\
        return fmt::format_to(ctx.out(), _Lambda_());\
    }\
};

#define TT_DEFINE_CUSTOM_FORMATTER_ENUM(_Type_)\
template<>\
struct fmt::formatter<_Type_>\
{\
    template<typename ParseContext>\
    constexpr auto parse(ParseContext& ctx)\
    { return ctx.begin(); }\
\
    template<typename FormatContext>\
    auto format(_Type_ const& InEnum, FormatContext& ctx)\
    {\
        return fmt::format_to(ctx.out(), UEnum::GetDisplayValueAsText(InEnum).ToString());\
    }\
};
