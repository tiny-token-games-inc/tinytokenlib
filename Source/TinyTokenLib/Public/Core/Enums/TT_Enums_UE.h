#pragma once

#include "TinyTokenLib/Public/Core/Format/TT_Format.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_CoreMinimal.h"

#include "TT_Enums_UE.generated.h"

// --------------------------------------------------------------------------------------------------------------------

UENUM(BlueprintType)
enum class ETT_Core_ValidInvalid : uint8
{
    Valid,
    Invalid
};

TT_DEFINE_CUSTOM_FORMATTER_ENUM(ETT_Core_ValidInvalid);

// --------------------------------------------------------------------------------------------------------------------

UENUM(BlueprintType)
enum class ETT_Core_RoundingMethod : uint8
{
    Ceiling,
    Floor,
    Closest UMETA(Description=">= 0.5 is ceiling and < 0.5 is floor")
};

TT_DEFINE_CUSTOM_FORMATTER_ENUM(ETT_Core_RoundingMethod);

// --------------------------------------------------------------------------------------------------------------------

UENUM(BlueprintType)
enum class ETT_Core_ZeroPositiveNegative : uint8
{
    Zero,
    Positive,
    Negative
};

TT_DEFINE_CUSTOM_FORMATTER_ENUM(ETT_Core_ZeroPositiveNegative);
