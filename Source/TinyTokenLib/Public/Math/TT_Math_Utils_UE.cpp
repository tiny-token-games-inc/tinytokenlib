#include "TT_Math_Utils_UE.h"

#include "TinyTokenLib/Public/Core/Ensure/TT_Ensure_UE.h"

// --------------------------------------------------------------------------------------------------------------------

auto
    UTT_Math_Utils_UE::
    Get_RoundFloatToFloat(ETT_Core_RoundingMethod InRoundingMethod,
                          float                   InValue)
    -> float
{
    return Get_RoundFloatToInt(InRoundingMethod, InValue);
}

auto
    UTT_Math_Utils_UE::
    Get_RoundFloatToInt(ETT_Core_RoundingMethod InRoundingMethod,
                        float                   InValue)
    -> float
{
    switch(InRoundingMethod)
    {
        case ETT_Core_RoundingMethod::Ceiling:
            return FMath::CeilToInt(InValue);
            break;
        case ETT_Core_RoundingMethod::Floor:
            return FMath::FloorToInt(InValue);
            break;
        case ETT_Core_RoundingMethod::Closest:
            return FMath::RoundToInt(InValue);
            break;
        default:
            tt::core::ensure::Ensuref_InvalidEnum(InRoundingMethod);
            return InValue;
    }
}
