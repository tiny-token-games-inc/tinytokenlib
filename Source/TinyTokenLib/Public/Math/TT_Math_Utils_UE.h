#pragma once

#include "TinyTokenLib/Public/Core/Macros/TT_Macros.h"
#include "TinyTokenLib/Public/Core/Enums/TT_Enums_UE.h"

#include "TinyTokenLib/Public/Engine/TT_Engine_BlueprintFunctionLibrary.h"

#include "TT_Math_Utils_UE.generated.h"

// --------------------------------------------------------------------------------------------------------------------

UCLASS()
class UTT_Math_Utils_UE : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

public:
    TT_GENERATED_BODY(UTT_Math_Utils_UE);

public:
    UFUNCTION(BlueprintPure)
    static float
    Get_RoundFloatToFloat(ETT_Core_RoundingMethod InRoundingMethod,
                          float                   InValue);

    UFUNCTION(BlueprintPure)
    static float
    Get_RoundFloatToInt(ETT_Core_RoundingMethod InRoundingMethod,
                        float                   InValue);
};
